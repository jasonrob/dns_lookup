<?php
	header("Access-Control-Allow-Origin: *");
	
	$host =  $_GET['host'];

	if (filter_var($host, FILTER_VALIDATE_IP)) {	
		$result = gethostbyaddr($host);
	} else {
		$result = gethostbyname($host);
	}
	
	echo $result;
?>
