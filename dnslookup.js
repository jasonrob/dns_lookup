browser.contextMenus.create({
  title: "Reverse Lookup IP", 
  contexts:["selection"],
  onclick: revlookup,
});

function revlookup(info,tab) {
    if(info){
    	var token=info.selectionText;
    	var revlookupUrl = 'https://web.uvic.ca/~jasonrob/revlookup.php?host='+token;
    	lookUpSelection(token, revlookupUrl);
   	}     
}

function lookUpSelection(text, url) {
    if(text){
		var w = 650;
        var h = 250;
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2);
 		browser.windows.create({'url': url, 
 		    'type': 'popup',
 		    'width': w,
 		    'height': h,
 		    'left': left, 
 		    'top': top }, function(window) {}
   		);
   	}  
}
